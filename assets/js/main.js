var baseURL = "/reciclus2";

const config = {
  game: {
    level: 1,

    spawn: {
      yCoord: -180
    },

    enemies: [
      { sprite: `${baseURL}/assets/img/objects/rock.png` },
      { sprite: `${baseURL}/assets/img/objects/rock.png` },
      { sprite: `${baseURL}/assets/img/objects/rock.png` },
    ],

    items: [
      { sprite: `${baseURL}/assets/img/objects/star.png` },
      { sprite: `${baseURL}/assets/img/objects/heart.png` }
    ],

    sounds: {
      track: {
        src: `${baseURL}/assets/audio/soundtrack.webm`,
        volume: 0.2
      },
      start: {
        src: `${baseURL}/assets/audio/effects/start.ogg`,
        volume: 0.5
      },
      gameover: {
        src: `${baseURL}/assets/audio/effects/gameover.ogg`,
        volume: 0.5
      },
      levelup: {
        src: `${baseURL}/assets/audio/effects/levelup.ogg`,
        volume: 0.5
      },
      collect: {
        src: `${baseURL}/assets/audio/effects/collect.ogg`,
        volume: 0.5
      },
      collision: {
        src: `${baseURL}/assets/audio/effects/collision.mp3`,
        volume: 1
      }
    }
  },

  player: {
    sprite: `${baseURL}/assets/img/entities/char/char-pink-girl.png`,

    lives: 5,
    points: 0,
    
    required_xp: 30,

    coords: {
      // x: 200,
      // y: 300,
    }
  },

  engine: {
    canvas: {
      container: document.getElementById("canvas_container"),
      element: document.createElement('canvas'),
    },

    row: {
      images: [
        `${baseURL}/assets/img/sprites/sky.png`,   // céu
        `${baseURL}/assets/img/sprites/sky.png`,   // céu
        `${baseURL}/assets/img/sprites/sky.png`,   // céu
        `${baseURL}/assets/img/sprites/sky.png`,   // céu

        `${baseURL}/assets/img/sprites/grass-block.png`,   // linha feita de grama
      ]
    },

    column: {
      count: 9
    }
  },

  resources: [
    `${baseURL}/assets/img/sprites/stone-block.png`,
    `${baseURL}/assets/img/sprites/sky.png`,
    `${baseURL}/assets/img/sprites/grass-block.png`,

    `${baseURL}/assets/img/objects/rock.png`,

    `${baseURL}/assets/img/objects/heart.png`,
    `${baseURL}/assets/img/objects/star.png`,
    
    `${baseURL}/assets/img/entities/char/char-pink-girl.png`
  ]
}

const reciclus = new Game(config);

window.onload = () => {
  
  reciclus.showStartScreen();
}
